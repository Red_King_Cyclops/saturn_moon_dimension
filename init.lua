--SKY--

--Version 0.2

pos = {x=0, y=0, z=0}

local space = 2000 --value for space, change the value to however you like.

--The skybox for space, feel free to change it to however you like.
local spaceskybox = {
"sky_pos_y.png^[transform3",
"sky_neg_y.png^[transform1",
"sky_pos_z.png",
"sky_neg_z.png",
"sky_neg_x.png",
"sky_pos_x.png",
}

local time = 0

minetest.register_globalstep(function(dtime)
time = time + dtime
if time > 1 then for _, player in ipairs(minetest.get_connected_players()) do
time = 0

local name = player:get_player_name()
local pos = player:getpos()
 
   --If the player has reached Space
   if minetest.get_player_by_name(name) and pos.y >= space then
   --player:set_physics_override(1, 0.6, 0.2) -- speed, jump, gravity
   --player:set_sky({}, "skybox", spaceskybox) -- Sets skybox
   player:set_clouds({density = 0})

   --If the player is on Earth
   elseif minetest.get_player_by_name(name) and pos.y < space then
   --player:set_physics_override(1, 1, 1) -- speed, jump, gravity [default]
   --player:set_sky({}, "regular", {}) -- Sets skybox, in this case it sets the skybox to it's default setting if and only if the player's Y value is less than the value of space.
   player:set_clouds({density = 0.4})
      
      end
         end
            end
               end)

minetest.register_on_leaveplayer(function(player)
local name = player:get_player_name()
   
   if name then

   player:set_sky({}, "regular", {})

         end
            end)

--MAPGEN--

local ores={
	["default:stone_with_coal"]=200,
	["default:stone_with_iron"]=400,
	["default:stone_with_copper"]=500,
	["default:stone_with_gold"]=2000,
	["default:stone_with_mese"]=10000,
	["default:stone_with_diamond"]=20000,
	["default:mese"]=40000,
	["default:gravel"]={chance=3000,chunk=2,}
}

multidimensions.clear_dimensions() -- clear all dimensions

multidimensions.register_dimension("saturn_moon",{

  ground_ores = {--[[
    ["default:tree"] = 1000,            -- (chance) ... spawns on ground, used by trees, grass, flowers...
    ["default:stone"] = {chance=1000}, 	-- same as above
    ["default:dirt_with_snow"] = {	-- names will be remade to content_id
    	chance=5000,	     -- chance
	min_heat=10,	     -- min heat
	max_heat=40,	     -- max heat
	chunk=3,	     -- chunk size
    },
  ]]},
  stone_ores = table.copy(ores),     	     -- works as above, but in stone
  dirt_ores = table.copy(ores),
  grass_ores = {},
  air_ores = {},
  water_ores = {},
  sand_ores = {},
  
  self = {},		    -- can contain everything, var like dirt="default:dirt" will be remade to dirt=content_id
  
  dim_y = 2000,             -- dimension start (don't change if you don't know what you're doing)
  dim_height =  1000,	    -- dimension height
  
  
  dirt_start = 501,           -- when dirt begins to appear (default is 501)
  dirt_depth = 1,	    -- dirt depth
  ground_limit = 530,	    -- ground y limit (ground ends here)
  water_depth = 8,	    -- depth fron ground and down
  enable_water = nil,       -- (nil = true)
  terrain_density = 0.4,    -- or ground density
  flatland = nil,           -- (nil = false)
  teleporter = false,         -- (nil = true) dimension teleporter
  gravity = 1,		    -- (1 = default) dimension gravity
  
  stone = "default:stone",
  dirt = "default:stone",
  grass = "default:gravel",
  air = "vacuum:vacuum",
  water = "vacuum:vacuum",
  sand = "default:gravel",
  bedrock = "multidimensions:bedrock", -- at dimension edges
  
  map = {
    offset = 0,
    scale = 1,
    spread = {x=100,y=18,z=100},
    seeddiff = 24,
    octaves = 5,
    persist = 0.7,
    lacunarity = 1,
    flags = "absvalue",
   },
   
   --[[
   craft = { -- teleport craft recipe
	{"default:obsidianbrick", "default:steel_ingot", "default:obsidianbrick"},
	{"default:wood","default:mese","default:wood",},
	{"default:obsidianbrick", "default:steel_ingot", "default:obsidianbrick"},
   },
   ]]

--[[
   on_generate=function(self,data,id,area,x,y,z)
	if y <= self.dirt_start+5 then
		data[id] = self.air
	else
		return
	end
	return data -- to return changes
   end,
   ]]
   
   -- data: active generating area (VoxelArea)
   -- index: data index
   -- self: {dim_start, dim_end, dim_height, ground_limit, heat, humidity, dirt, stone, grass, air, water, sand, bedrock ... and your inputs
    ----area: (VoxelArea:new({MinEd...})
   
   --sky = {{r=219, g=168, b=117},"plain",{}}, -- same as:set_sky()
   sky = {{}, "skybox", spaceskybox}, -- same as:set_sky()
   
   on_enter=function(player) --on enter dimension
   end,
   
   on_leave=function(player) --on leave dimension
   end,
   
})
