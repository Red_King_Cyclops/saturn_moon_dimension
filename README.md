# saturn_moon_dimension

This mod is the dimension version of saturn_moon. Instead of replacing Earth with a moon of Saturn, a saturn_moon dimension can be found in the sky. At the moment, the best way to reach it is to use the jumpdrive mod.

**Dependencies**
* default
* multidimensions
* vacuum

Other mods are recommended, such as jumpdrive and spacesuit.

The code is licenced as LGPL-2.1 or later and the textures as CC-BY-SA 3.0. Code written by AiTechEye in multidimensions has been borrowed. Without multidimensions, this mod would not have been as feasible. The saturn textures are from Minetest Saturn by Foghrye4.